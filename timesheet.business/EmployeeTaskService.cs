﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
   public class EmployeeTaskService:IEmployeeTask
    {
        public TimesheetDb db { get; }
        public EmployeeTaskService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public void Create(EmployeeTask EmployeeTask)
        {
            db.Add(EmployeeTask);
            db.SaveChanges();
        }

        public void Delete(EmployeeTask EmployeeTask)
        {
            throw new NotImplementedException();
        }

        public IQueryable<EmployeeTask> GetAll()
        {
            return this.db.EmployeeTask;
        }

        public IEnumerable<EmployeeTask> EmployeeTaskFindByCondition(Expression<Func<EmployeeTask, bool>> expression)
        {
            return this.db.Set<EmployeeTask>().Where(expression);
        }
    }
}

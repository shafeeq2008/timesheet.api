﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using timesheet.model;

namespace timesheet.business
{
  public  interface IEmployeeTask
    {
        void Create(EmployeeTask EmployeeTask);
        void Delete(EmployeeTask EmployeeTask);
        IQueryable<EmployeeTask> GetAll();
        IEnumerable<EmployeeTask> EmployeeTaskFindByCondition(Expression<Func<EmployeeTask, bool>> expression);


    }
}

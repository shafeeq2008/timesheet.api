﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService:IEmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IEnumerable<ReportEmployee> GetEmployees()
        {
            return db.Query<ReportEmployee>().FromSql("EmployeeList").ToList();

        }
    }
}

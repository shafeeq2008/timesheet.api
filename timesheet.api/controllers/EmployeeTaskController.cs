﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace timesheet.api.controllers
{
    [Route("api/v1/employeetask")]
    [ApiController]
    public class EmployeeTaskController : ControllerBase
    {
        private readonly IEmployeeTask employeeTaskService;
        public EmployeeTaskController(IEmployeeTask employeeTaskService)
        {
            this.employeeTaskService = employeeTaskService;
        }

        [HttpPost("Create")]
        public IActionResult create([FromBody] EmployeeTask _EmployeeTask)
        {
           // _EmployeeTask.EmployeeId = 2;
            employeeTaskService.Create(_EmployeeTask);
            return Ok(new { StatusCode = 1, Data = "", Message = " successfully  Saved" });
        }



        [HttpGet("GetTasklist")]
        public IActionResult getTasklist(int empid)
        {
          
            return Ok(employeeTaskService.EmployeeTaskFindByCondition(x => x.EmployeeId == empid));

        }






        }
}

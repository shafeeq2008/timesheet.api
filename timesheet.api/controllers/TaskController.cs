﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace timesheet.api.controllers
{
    [Route("api/v1/task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly  Itask itask;
        public TaskController(Itask _task)
        {
            this.itask = _task;
        }

     
        [HttpGet("GetTask")]
        public IActionResult getTask()
        {

            return Ok(itask.GetAll());

        }
    }
}

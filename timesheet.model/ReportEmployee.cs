﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
  public  class ReportEmployee
    {
        public int Id { get; set; }
     
        public string Code { get; set; }      
        public string Name { get; set; }
      
        public int TotalWeelyEffort { get; set; }
        
        public int AvgWeelyEffort { get; set; }
    }
}
